#ifndef GAMEPRESENTATIONINTERFACE_H
#define GAMEPRESENTATIONINTERFACE_H

#include <QObject>

class GamePresentationInterface {
public:
    virtual ~GamePresentationInterface(){}

public slots:
    virtual void resetPresentation() = 0;
    virtual void incorrectButtons(int, int) = 0;
    virtual void correctButton(int) = 0;
    virtual void showMessageBox() = 0;
};

Q_DECLARE_INTERFACE(GamePresentationInterface, "GamePresentationInterface")

#endif // GAMEPRESENTATIONINTERFACE_H
