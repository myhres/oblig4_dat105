#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include <QWidget>

#include <random>
#include <ctime>
#include <cstdlib>

#include "gamelogicinterface.h"
#include "gamepresentationinterface.h"

class GameLogic : public QWidget, public GameLogicInterface {
    Q_OBJECT
    Q_INTERFACES(GameLogicInterface)

    int gameSize;
    int guessValue; // Value to be guessed

    int generateRandom(); // Returns new random value
    int generateRandom_c();

public:
    GameLogic(int);
    ~GameLogic();

    void connectSignals(GamePresentationInterface*);

signals:
    void newGameStarted(); // A new game has started
    void guessedIncorrect(int, int); // User guessed incorrectly
    void guessedCorrect(int); // User guessed correct

public slots:
    void newGame(); // Starts a new game
    void guess(int); // Checks guess from user
};

#endif // GAMELOGIC_H
