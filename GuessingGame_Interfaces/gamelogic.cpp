#include "gamelogic.h"

GameLogic::GameLogic(int gameSize) : gameSize(gameSize) {
    guessValue = generateRandom_c();
}

GameLogic::~GameLogic() {  }

// Connects signals from this class to interface
void GameLogic::connectSignals(GamePresentationInterface* gpi) {
    connect(this, SIGNAL(newGameStarted()),
            dynamic_cast<QObject*>(gpi), SLOT(resetPresentation()));

    connect(this, SIGNAL(guessedIncorrect(int, int)),
            dynamic_cast<QObject*>(gpi), SLOT(incorrectButtons(int, int)));

    connect(this, SIGNAL(guessedCorrect(int)),
            dynamic_cast<QObject*>(gpi), SLOT(correctButton(int)));

    connect(this, SIGNAL(guessedCorrect(int)),
            dynamic_cast<QObject*>(gpi), SLOT(showMessageBox()));
}

// Slot for new game
void GameLogic::newGame() {
    guessValue = generateRandom_c();

    emit newGameStarted();
}

// Slot for new guess
void GameLogic::guess(int guessedValue) {
    if(guessedValue < guessValue) {
        emit guessedIncorrect(0, guessedValue);
    }
    else if (guessedValue > guessValue) {
        emit guessedIncorrect(guessedValue, gameSize-1);
    }
    else {
        emit guessedCorrect(guessedValue);
    }
}

// Generates a new random number
int GameLogic::generateRandom() {
    /* Fikk beskjed etter oblig 3 at denne
     * metoden ikke returnerer tilfeldige tall
     * korrekt. Har selv ikke hatt problemer med denne,
     * jeg får ulike tall hver gang.
     * Har derfor implementert en egen metode som
     * genererer tilfeldig tall på C-måten også;
     * GameLogic::generateRandom_c()
     */
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(0, gameSize-1);

    return dist(mt);
}

int GameLogic::generateRandom_c() {
    srand(time(NULL));
    return rand() % gameSize;
}
