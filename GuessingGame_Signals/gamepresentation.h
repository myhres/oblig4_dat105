#ifndef GAMEPRESENTATION_H
#define GAMEPRESENTATION_H

#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSignalMapper>
#include <QMessageBox>

#include <vector>

class GamePresentation : public QWidget {
    Q_OBJECT

    QHBoxLayout *hbox;
    QPushButton *newGameBtn;
    std::vector<QPushButton*> buttons;
    QSignalMapper *signalMapper;
    QMessageBox *messageBox;

    const int count = 10; // number of buttons

public:
    GamePresentation(QWidget *parent = 0);
    ~GamePresentation();

    // Getters
    QPushButton *getNewGameBtn();
    QSignalMapper *getSignalMapper();
    int getCount();

public slots:
    void resetPresentation(); // oppdaterer slik at presentation er klar for nytt spill
    void incorrectButtons(int, int); // gjør knapper som er feil røde
    void correctButton(int); // gjør rett knapp grønn
    void showMessageBox(); // Shows a messagebox
};

#endif // GAMEPRESENTATION_H
