#ifndef MAILCLIENTVIEWINTERFACE_H
#define MAILCLIENTVIEWINTERFACE_H

#include <QObject>

class MailClientViewInterface {
public:
    virtual ~MailClientViewInterface(){}

public slots:

};

Q_DECLARE_INTERFACE(MailClientViewInterface, "MailClientViewInterface")

#endif // MAILCLIENTVIEWINTERFACE_H
